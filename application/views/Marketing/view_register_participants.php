<div class="container panel panel-group">
<form class="form-horizontal" method="" role="form" id="form_order" name="form_order">
<div class="row panel panel-primary">
	<div class="panel-heading"><p>Order<button class="btn btn-primary input_btn-copy" id="input_btn-copy_nasabah" style="float: right;">-</button></p></div>
	<div class="form-group" id="">
		<div class="form-group"></div>
		<div class="form-group">
			<div class="row">
				<div class="col-sm-1 text-right"></div>
				<div class="col-sm-2"><label class="control-label">Number Order</label></div>
				<div class="col-sm-4"><input class="form-control" type="textfield" name="order_number" id="order_number"></div>
			</div>
		</div>
		<div class="form-group">
			<div class="row">
				<div class="col-sm-1 text-right"><label class="control-label"></label></div>
				<div class="col-sm-2"><label class="control-label">Donation Package</label></div>
				<div class="col-sm-4">
					<select class="form-control pilihan" name="order_package">
						<option value="" selected disabled>--Order Package--</option>
						<?php
							foreach($donasi as $result_donasi)
							{
								echo "<option value='".$result_donasi['donasis']."'>".$result_donasi['donasis']."</option>";
							}
						?>
		            	<option value="Lain">Lainnya</option>
		            </select>
		        </div>
		    </div>
		</div>
		<div class="form-group">
		<div class="row">
			<div class="col-sm-2 text-right">
			</div>
			<div class="col-sm-2">
				<button type="submit" class="btn btn-primary" id="submit" name="submit"><span class="glyphicon glyphicon-floppy-disk"></span> Simpan</button>
			</div>
		</div>
	</div>
</div>
</form>
</div>
<script type="text/javascript">
$("#form_order").on("submit", function (e) {
        e.preventDefault();
        $.ajax({
          url: '<?php echo base_url("Marketing/main/insert"); ?>',
          type: 'POST',
          data: $('#form_order').serialize(),
          success: function(data){
            console.log(data);
            if('Sukses')
            {
            	console.log(data);
            	alert('Data Berhasil Disimpan');
            	location.href = '<?php echo base_url("Marketing/main/"); ?>';
            }
          }
        });
      });
</script>