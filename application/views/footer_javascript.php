<script type="text/javascript">
$(document).ready(function(){

	$(".tanggalan").datepicker({
    dateFormat: 'dd/mm/yy',
    yearRange: "1915:2015",
    changeYear: true,
    changeMonth:true,

  });
  $(".tanggalan").attr("readonly","readonly");



  $(".pilihan").change(function(){

    if ($(this).val() == 'Lain') {
    	console.log($(this).attr("name"));
      $(this).parent().parent().append("<div class='col-md-2' id='"+$(this).attr("name")+"pilihan'><input class='form-control' type='textfield' id='lain_"+$(this).attr("name")+"' name='lain_"+$(this).attr("name")+"' placeholder='Data Lainnya'></div>");        
    }
    else
    {
      $('#'+$(this).attr("name")+"pilihan").remove();
    }
  });
});
</script>