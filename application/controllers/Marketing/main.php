<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Main extends CI_Controller {

	public function index()
	{
		$all_data = $this->retrieve_data();

		foreach($all_data as $result_data)
		{
			$arr[]['donasis'] = $result_data;
		}

        $data['donasi'] = $arr;
		$this->load->view('header_file');
		$this->load->view('Marketing/view_register_participants',$data);
		$this->load->view('footer_javascript');
	}

	public function retrieve_data()
	{
		$data = array(	'package_1' => '5',
						'package_2' => '10',
						'package_3' => '15',
						'package_4' => '20');
		return $data;
	}
}